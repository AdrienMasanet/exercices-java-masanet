package com.car;

public class Car {

    String brand;
    String model;
    String engine;
    Integer doors;
    Integer gears;
    Integer mileage;
    boolean isOccas;
    Integer stock;

    public Car(String brand, String model, Integer doors, Integer gears, Integer mileage, String engine, boolean isOccas, Integer stock) {
        this.brand = brand;
        this.model = model;
        this.engine = engine;
        this.doors = doors;
        this.gears = gears;
        this.mileage = mileage;
        this.isOccas = isOccas;
        this.stock = stock;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public Integer getMileage() {
        return mileage;
    }

    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }

    public boolean isOccas() {
        return isOccas;
    }

    public Integer getDoors() {
        return doors;
    }

    public void setDoors(Integer doors) {
        this.doors = doors;
    }

    public Integer getGears() {
        return gears;
    }

    public void setGears(Integer gears) {
        this.gears = gears;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public boolean getIsOccas() {
        return isOccas;
    }

    public void setOccas(boolean occas) {
        isOccas = occas;
    }

    @Override
    public String toString() {
        return "Car : \n" +
                "mileage ='" + this.getMileage() + "'\n" +
                "brand ='" + this.getBrand() + "'\n" +
                "model ='" + this.getModel() + "'\n" +
                "engine ='" + this.getEngine() + "'\n" +
                "doors ='" + this.doors + "'\n" +
                "gears ='" + this.gears + "'\n" +
                "stock ='" + this.stock + "'\n" +
                "stock ='" + this.stock + "'\n";
    }
}
