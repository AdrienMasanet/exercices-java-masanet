import javax.swing.*;
import java.awt.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.AbstractTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.car.Car;

public class CarsGui extends AbstractTableModel {

    // Path to the JSON file containing data
    final String jsonPath = getClass().getResource("myCarsJson.json").getPath();
    // Base window elements
    JFrame frame;
    JTable table;
    JButton addButton;
    JPanel panel;
    JScrollPane scrollPane;
    // New car form window elements
    JFrame formFrame;
    JPanel newCarFieldsPanel;
    JTextField brandField;
    JLabel brandFieldLabel;
    JPanel newCarBrandFieldPanel;
    JTextField modelField;
    JLabel modelFieldLabel;
    JPanel newCarModelFieldPanel;
    JSpinner doorsField;
    JLabel doorsFieldLabel;
    JPanel newCarDoorsFieldPanel;
    JSpinner gearsField;
    JLabel gearsFieldLabel;
    JPanel newCarGearsFieldPanel;
    JSpinner mileageField;
    JLabel mileageFieldLabel;
    JPanel newCarMileageFieldPanel;
    JTextField engineField;
    JLabel engineFieldLabel;
    JPanel newCarEngineFieldPanel;
    JButton newCarSubmitButton;
    JCheckBox isOccasField;
    JLabel isOccasFieldLabel;
    JPanel newCarIsOccasFieldPanel;
    JSpinner stockField;
    JLabel stockFieldLabel;
    JPanel newCarStockFieldPanel;
    JPanel newCarSubmitButtonPanel;

    List<Car> cars;

    String[] headers = {"Brand", "Model", "Doors", "Gears", "New", "Stock"};

    // Empty constructor for first instantiation
    public CarsGui() {
    }

    // Constructor with cars table for gui initialization
    public CarsGui(List<Car> cars) {
        this.cars = cars;
    }

    public static void main(String[] args) {
        new CarsGui().initGui();
    }

    private void initGui() {
        // Base application window
        frame = new JFrame("Car stock");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);

        // Table with parsed cars data that are in the json
        table = new JTable(new CarsGui(parseJson()));

        // Add a car button
        addButton = new JButton("Add a car");
        addButton.addActionListener(new openNewCarForm());

        // Button panel
        panel = new JPanel();
        panel.add(addButton);
        frame.getContentPane().add(panel, BorderLayout.NORTH);

        // Scroll pane
        scrollPane = new JScrollPane(table);
        frame.getContentPane().add(scrollPane, BorderLayout.CENTER);

        // Set base application window visible
        frame.setVisible(true);

        // New car form window
        formFrame = new JFrame("Create a new car");
        formFrame.setPreferredSize(new Dimension(300, 500));
        centerWindow(formFrame);
        formFrame.pack();
        formFrame.setLocationRelativeTo(null);
        formFrame.setVisible(false);

        // New car form brand field, label & panel
        brandField = new JTextField();
        brandField.setColumns(15);
        brandField.setMaximumSize(brandField.getPreferredSize());
        brandFieldLabel = new JLabel("Brand");
        brandFieldLabel.setBorder(new EmptyBorder(0, 0, 0, 10));
        newCarBrandFieldPanel = new JPanel();
        newCarBrandFieldPanel.setLayout(new BoxLayout(newCarBrandFieldPanel, BoxLayout.X_AXIS));
        newCarBrandFieldPanel.setBorder(new EmptyBorder(20, 0, 5, 0));
        newCarBrandFieldPanel.add(brandFieldLabel);
        newCarBrandFieldPanel.add(brandField);
        formFrame.getContentPane().add(newCarBrandFieldPanel);

        // New car form model field, label & panel
        modelField = new JTextField();
        modelField.setColumns(15);
        modelField.setMaximumSize(modelField.getPreferredSize());
        modelFieldLabel = new JLabel("Model");
        modelFieldLabel.setBorder(new EmptyBorder(0, 0, 0, 10));
        newCarModelFieldPanel = new JPanel();
        newCarModelFieldPanel.setLayout(new BoxLayout(newCarModelFieldPanel, BoxLayout.X_AXIS));
        newCarModelFieldPanel.setBorder(new EmptyBorder(5, 0, 5, 0));
        newCarModelFieldPanel.add(modelFieldLabel);
        newCarModelFieldPanel.add(modelField);
        formFrame.getContentPane().add(newCarModelFieldPanel);

        // New car form doors field, label & panel
        doorsField = new JSpinner();
        doorsField.setMaximumSize(doorsField.getPreferredSize());
        doorsFieldLabel = new JLabel("Doors");
        doorsFieldLabel.setBorder(new EmptyBorder(0, 0, 0, 10));
        newCarDoorsFieldPanel = new JPanel();
        newCarDoorsFieldPanel.setLayout(new BoxLayout(newCarDoorsFieldPanel, BoxLayout.X_AXIS));
        newCarDoorsFieldPanel.setBorder(new EmptyBorder(5, 0, 20, 0));
        newCarDoorsFieldPanel.add(doorsFieldLabel);
        newCarDoorsFieldPanel.add(doorsField);
        formFrame.getContentPane().add(newCarDoorsFieldPanel);

        // New car form gears field, label & panel
        gearsField = new JSpinner();
        gearsField.setMaximumSize(gearsField.getPreferredSize());
        gearsFieldLabel = new JLabel("Gears");
        gearsFieldLabel.setBorder(new EmptyBorder(0, 0, 0, 10));
        newCarGearsFieldPanel = new JPanel();
        newCarGearsFieldPanel.setLayout(new BoxLayout(newCarGearsFieldPanel, BoxLayout.X_AXIS));
        newCarGearsFieldPanel.setBorder(new EmptyBorder(5, 0, 20, 0));
        newCarGearsFieldPanel.add(gearsFieldLabel);
        newCarGearsFieldPanel.add(gearsField);
        formFrame.getContentPane().add(newCarGearsFieldPanel);

        // New car form mileage field, label & panel
        mileageField = new JSpinner();
        mileageField.setMaximumSize(mileageField.getPreferredSize());
        mileageFieldLabel = new JLabel("Mileage");
        mileageFieldLabel.setBorder(new EmptyBorder(0, 0, 0, 10));
        newCarMileageFieldPanel = new JPanel();
        newCarMileageFieldPanel.setLayout(new BoxLayout(newCarMileageFieldPanel, BoxLayout.X_AXIS));
        newCarMileageFieldPanel.setBorder(new EmptyBorder(5, 0, 20, 0));
        newCarMileageFieldPanel.add(mileageFieldLabel);
        newCarMileageFieldPanel.add(mileageField);
        formFrame.getContentPane().add(newCarMileageFieldPanel);

        // New car form engine field, label & panel
        engineField = new JTextField();
        engineField.setColumns(15);
        engineField.setMaximumSize(engineField.getPreferredSize());
        engineFieldLabel = new JLabel("Engine");
        engineFieldLabel.setBorder(new EmptyBorder(0, 0, 0, 10));
        newCarEngineFieldPanel = new JPanel();
        newCarEngineFieldPanel.setLayout(new BoxLayout(newCarEngineFieldPanel, BoxLayout.X_AXIS));
        newCarEngineFieldPanel.setBorder(new EmptyBorder(5, 0, 5, 0));
        newCarEngineFieldPanel.add(engineFieldLabel);
        newCarEngineFieldPanel.add(engineField);
        formFrame.getContentPane().add(newCarEngineFieldPanel);

        // New car form isOccas field, label & panel
        isOccasField = new JCheckBox();
        isOccasFieldLabel = new JLabel("Is occasion");
        isOccasFieldLabel.setBorder(new EmptyBorder(0, 0, 0, 10));
        newCarIsOccasFieldPanel = new JPanel();
        newCarIsOccasFieldPanel.setLayout(new BoxLayout(newCarIsOccasFieldPanel, BoxLayout.X_AXIS));
        newCarIsOccasFieldPanel.setBorder(new EmptyBorder(5, 0, 20, 0));
        newCarIsOccasFieldPanel.add(isOccasFieldLabel);
        newCarIsOccasFieldPanel.add(isOccasField);
        formFrame.getContentPane().add(newCarIsOccasFieldPanel);

        // New car form stock field, label & panel
        stockField = new JSpinner();
        stockField.setMaximumSize(stockField.getPreferredSize());
        stockFieldLabel = new JLabel("Stock");
        stockFieldLabel.setBorder(new EmptyBorder(0, 0, 0, 10));
        newCarStockFieldPanel = new JPanel();
        newCarStockFieldPanel.setLayout(new BoxLayout(newCarStockFieldPanel, BoxLayout.X_AXIS));
        newCarStockFieldPanel.setBorder(new EmptyBorder(5, 0, 20, 0));
        newCarStockFieldPanel.add(stockFieldLabel);
        newCarStockFieldPanel.add(stockField);
        formFrame.getContentPane().add(newCarStockFieldPanel);

        // New car form fields panel
        newCarFieldsPanel = new JPanel();
        formFrame.getContentPane().add(newCarFieldsPanel);
        newCarFieldsPanel.setLayout(new BoxLayout(newCarFieldsPanel, BoxLayout.Y_AXIS));
        newCarFieldsPanel.add(newCarBrandFieldPanel);
        newCarFieldsPanel.add(newCarModelFieldPanel);
        newCarFieldsPanel.add(newCarDoorsFieldPanel);
        newCarFieldsPanel.add(newCarGearsFieldPanel);
        newCarFieldsPanel.add(newCarMileageFieldPanel);
        newCarFieldsPanel.add(newCarIsOccasFieldPanel);
        newCarFieldsPanel.add(newCarStockFieldPanel);
        newCarFieldsPanel.add(newCarEngineFieldPanel);

        // New car form submit button
        newCarSubmitButton = new JButton("Create new car");
        newCarSubmitButton.addActionListener(new submitNewCarForm());

        // New car form submit button panel
        newCarSubmitButtonPanel = new JPanel();
        formFrame.getContentPane().add(newCarSubmitButtonPanel, BorderLayout.SOUTH);
        newCarSubmitButtonPanel.setBorder(new EmptyBorder(0, 0, 40, 0));
        newCarSubmitButtonPanel.add(newCarSubmitButton);
    }

    public List<Car> parseJson() {
        cars = new ArrayList<>();
        JSONParser jsonParser = new JSONParser();

        try {
            FileReader fileReader = new FileReader(jsonPath);
            Object object = jsonParser.parse(fileReader);
            JSONArray jsonArray = (JSONArray) object;

            jsonArray.forEach(
                    car -> {
                        JSONObject myCar = (JSONObject) car;
                        Car newCar = new Car(
                                (String) myCar.get("brand"),
                                (String) myCar.get("model"),
                                ((Long) myCar.get("doors")).intValue(),
                                ((Long) myCar.get("gears")).intValue(),
                                ((Long) myCar.get("mileage")).intValue(),
                                (String) myCar.get("engine"),
                                (Boolean) myCar.get("isOccas"),
                                ((Long) myCar.get("stock")).intValue()
                        );
                        cars.add(newCar);
                    });
        } catch (IOException | ParseException | NullPointerException exception) {
            exception.printStackTrace();
        }

        return cars;
    }

    @Override
    public int getRowCount() {
        return cars.size();
    }

    @Override
    public int getColumnCount() {
        return headers.length;
    }

    @Override
    public String getColumnName(int column) {
        return headers[column];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return switch (columnIndex) {
            case 0 -> cars.get(rowIndex).getBrand();
            case 1 -> cars.get(rowIndex).getModel();
            case 2 -> cars.get(rowIndex).getDoors();
            case 3 -> cars.get(rowIndex).getGears();
            case 4 -> cars.get(rowIndex).getIsOccas();
            case 5 -> cars.get(rowIndex).getStock();
            default -> null;
        };
    }

    private class openNewCarForm implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            formFrame.setVisible(true);
        }
    }

    private class submitNewCarForm implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            addCarToJson();
            table.setModel(new CarsGui(parseJson()));
            brandField.setText("");
            modelField.setText("");
            doorsField.setValue(0);
            gearsField.setValue(0);
            mileageField.setValue(0);
            engineField.setText("");
            isOccasField.setSelected(false);
            stockField.setValue(0);
            formFrame.setVisible(false);
        }
    }

    public void addCarToJson() {
        cars.add(new Car(
                brandField.getText(),
                modelField.getText(),
                (Integer) doorsField.getValue(),
                (Integer) gearsField.getValue(),
                (Integer) mileageField.getValue(),
                engineField.getText(),
                isOccasField.isSelected(),
                (Integer) stockField.getValue()
        ));

        try (FileWriter file = new FileWriter(jsonPath)) {

            JSONArray jsonCars = new JSONArray();

            for (Car car : cars) {
                JSONObject jsonCar = new JSONObject();
                jsonCar.put("brand", car.getBrand());
                jsonCar.put("model", car.getModel());
                jsonCar.put("doors", car.getDoors());
                jsonCar.put("gears", car.getGears());
                jsonCar.put("mileage", car.getMileage());
                jsonCar.put("engine", car.getEngine());
                jsonCar.put("isOccas", car.getIsOccas());
                jsonCar.put("stock", car.getStock());
                jsonCars.add(jsonCar);
            }

            file.write(jsonCars.toJSONString());
            file.flush();

        } catch (IOException | NullPointerException exception) {
            exception.printStackTrace();
        }
    }

    public static void centerWindow(Window frame) {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
        frame.setLocation(x, y);
    }
}
