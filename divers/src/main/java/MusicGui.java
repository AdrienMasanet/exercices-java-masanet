import javax.sound.midi.*;
import javax.swing.*;
import java.awt.*;

public class MusicGui {
    static JFrame frame = new JFrame("Color and Music");
    static DrawPanel drawPanel;

    public static void main(String[] args) {
        MusicGui music = new MusicGui();
        music.play();
    }

    public void initializeGui() {
        drawPanel = new DrawPanel();
        frame.setContentPane(drawPanel);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setBounds((screenSize.width / 2) - 250, (screenSize.height / 2) - 250, 500, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    public void play() {
        initializeGui();
        try {
            Sequencer sequencer = MidiSystem.getSequencer();
            sequencer.open();
            int[] events = {95};
            sequencer.addControllerEventListener(drawPanel, events);
            Sequence sequence = new Sequence(Sequence.PPQ, 4);
            Track track = sequence.createTrack();

            int randomization;
            for (int i = 35; i < 80; i += 4) {
                //randomization = (int) ((Math.random() * 50) + 1);
                track.add(makeEvent(144, 1, i, 100, i));
                track.add(makeEvent(128, 1, i, 100, i));
                track.add(makeEvent(176, 1, 95, 100, i));
            }

            sequencer.setSequence(sequence);
            sequencer.setTempoInBPM(220);
            sequencer.start();

        } catch (MidiUnavailableException | InvalidMidiDataException exception) {
            exception.printStackTrace();
        }
    }

    public static class DrawPanel extends JPanel implements ControllerEventListener {

        boolean paintTrigger = false;

        @Override
        public void controlChange(ShortMessage event) {
            paintTrigger = true;
            repaint();
        }

        @Override
        public void paintComponent(Graphics graphics) {
            if (paintTrigger) {
                int red = (int) (Math.random() * 250);
                int green = (int) (Math.random() * 250);
                int blue = (int) (Math.random() * 250);

                graphics.setColor(new Color(red, green, blue));

                int x = (int) ((Math.random() * 40) + 10);
                int y = (int) ((Math.random() * 40) + 10);
                int width = (int) ((Math.random() * 120) + 10);
                int height = (int) ((Math.random() * 120) + 10);

                graphics.fillRect(x, y, width, height);

                paintTrigger = false;
            }
        }

        public void repaint() {

        }
    }

    private MidiEvent makeEvent(int cmd, int chan, int done, int dtwo, int tick) {
        MidiEvent event = null;

        try {
            ShortMessage a = new ShortMessage();
            a.setMessage(cmd, chan, done, dtwo);
            event = new MidiEvent(a, (cmd == 128) ? tick + 16 : tick);
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return event;
    }
}
