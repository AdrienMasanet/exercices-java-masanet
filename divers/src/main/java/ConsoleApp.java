import java.util.Scanner;

public class ConsoleApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Veuillez saisir un mot");
        String input = scanner.nextLine();
        System.out.println("Vous avez saisi : " + input);
    }
}
