import java.util.Random;

import com.Animals.*;

public class Animal {
    static Random random = new Random();

    static com.Animals.Animal draw() {
        return Animal.random.nextInt(2) > 0 ? new Cat() : new Dog();
    }

    public static void main(String[] args) {
        com.Animals.Animal animal = Animal.draw();
        System.out.println("Le résultat est : " + animal.getName());
        animal.action();
    }
}
