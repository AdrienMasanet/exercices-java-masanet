import java.io.FileInputStream;
import java.util.Scanner;
import java.io.File;
import java.io.FileWriter;

public class SpyList {
    final private String path;
    private Scanner scanner;

    public SpyList() {
        this.path = "/home/stage/IdeaProjects/Exercices_java/src/data.csv";
    }

    public static void main(String[] args) {
        SpyList spyList = new SpyList();
        spyList.printCsv();
        spyList.addManuallyEntryToCsv();
    }

    public void printCsv() {
        try {
            FileInputStream inputStream = new FileInputStream(this.path);
            this.scanner = new Scanner(inputStream);

            String segment = "-------------------------------";
            System.out.printf("+%s+%s+%s+%s+\n", segment, segment, segment, segment);

            while (this.scanner.hasNextLine()) {
                String line = this.scanner.nextLine();
                String[] elems = line.split(";");
                System.out.printf("| %-30s| %-30s| %-30s| %-30s|\n", elems);
                System.out.printf("+%s+%s+%s+%s+\n", segment, segment, segment, segment);
            }

        } catch (
                Exception e) {
            System.out.println((e.getMessage()));
        }

    }

    public void addManuallyEntryToCsv() {

        this.scanner = new Scanner(System.in);

        System.out.println("Entrez l'ID de la personne : ");
        String newEntryId = this.scanner.nextLine();

        System.out.println("Entrez le prénom de la personne : ");
        String newEntryFirstName = this.scanner.nextLine();

        System.out.println("Entrez le nom de la personne : ");
        String newEntryLastName = this.scanner.nextLine();

        System.out.println("Entrez l'email de la personne : ");
        String newEntryEmail = this.scanner.nextLine();

        try {

            File file = new File(this.path);
            FileWriter writer = new FileWriter(file, true);
            writer.write("\n" + newEntryId + ";" + newEntryFirstName + ";" + newEntryLastName + ";" + newEntryEmail);
            writer.flush();

            System.out.println("Nouvelle entrée ajoutée dans le tableau :\n");
            String segment = "-------------------------------";
            System.out.printf("+%s+%s+%s+%s+\n", segment, segment, segment, segment);
            System.out.printf("| %-30s| %-30s| %-30s| %-30s|\n", new String[]{newEntryId, newEntryFirstName, newEntryLastName, newEntryEmail});
            System.out.printf("+%s+%s+%s+%s+\n", segment, segment, segment, segment);

        } catch (Exception e) {
            System.out.println((e.getMessage()));
        }
    }
}
