import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class JavaGui {

    JLabel label;
    JPanel formPanel;
    JFrame frame;

    public static void main(String[] args) {
        JavaGui javaGui = new JavaGui();
        javaGui.openWindow();
    }

    public void openWindow() {
        // Fenêtre principale de l'application
        frame = new JFrame("Yeaaaaah man mates ça pélo");
        frame.setSize(640, 480);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

        // Menu
        JPanel panelMenu = new JPanel();
        panelMenu.setBackground(Color.DARK_GRAY);
        panelMenu.setLayout(new BoxLayout(panelMenu, BoxLayout.X_AXIS));
        frame.getContentPane().add(BorderLayout.NORTH, panelMenu);

        // Bouton changement label
        JButton button1 = new JButton("Faire ceci");
        button1.addActionListener(new ChangeLabel());
        panelMenu.add(button1);

        // Bouton formulaire
        JButton button2 = new JButton("Faire cela");
        button2.addActionListener(new LoadForm());
        panelMenu.add(button2);

        // Contenu de la fenêtre
        label = new JLabel();
        label.setText("Abydefi hujon inene mav atewuwi popytyai");
        label.setHorizontalAlignment(SwingConstants.CENTER);
        frame.getContentPane().add(BorderLayout.CENTER, label);
    }

    private class ChangeLabel implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            label.setText("Label changé !");
        }
    }

    private class LoadForm implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            loadForm();
        }

        private void loadForm() {
            formPanel = new JPanel();
            formPanel.setPreferredSize(new Dimension(300, 300));
            Box formLayout = new Box(BoxLayout.Y_AXIS);

            JTextField lastName = new JTextField(10);
            formPanel.add(lastName);
            JTextField firstName = new JTextField(10);
            formPanel.add(firstName);
            JTextField age = new JTextField(10);
            formPanel.add(age);

            formPanel.add(formLayout);
            frame.getContentPane().add(formPanel, BorderLayout.EAST);
            frame.revalidate();
        }
    }
}
