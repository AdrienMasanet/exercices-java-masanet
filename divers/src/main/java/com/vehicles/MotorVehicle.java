package com.vehicles;

public abstract class MotorVehicle {
    private int wheels;
    private int mileage;
    private String brand;
    private String model;
    private String engine;
    private double fuel;
    private double brakePower;

    public MotorVehicle(int wheels, int mileage, String brand, String model, String engine, double fuel, double brakePower) {
        this.wheels = wheels;
        this.mileage = mileage;
        this.brand = brand;
        this.model = model;
        this.engine = engine;
        this.fuel = fuel;
        this.brakePower = brakePower;
    }

    public void refuel(double newFuel) {
        this.fuel += newFuel;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public double getFuel() {
        return fuel;
    }

    public void setFuel(double fuel) {
        this.fuel = fuel;
    }

    public double getBrakePower() {
        return brakePower;
    }

    public void setBrakePower(double brakePower) {
        this.brakePower = brakePower;
    }

    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

}
