package com.vehicles;

public class Car extends MotorVehicle {
    Integer doors;
    Integer gears;
    Integer stock;
    boolean isOccas;

    public Car(String brand, String model, Integer doors, Integer gears, int mileage, String engine, double fuel, double brakePower, boolean isOccas, Integer stock) {
        super(4, mileage, brand, model, engine, fuel, brakePower);
        this.doors = doors;
        this.gears = gears;
        this.isOccas = isOccas;
        this.stock = stock;
    }

    public Integer getDoors() {
        return doors;
    }

    public void setDoors(Integer doors) {
        this.doors = doors;
    }

    public Integer getGears() {
        return gears;
    }

    public void setGears(Integer gears) {
        this.gears = gears;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public boolean getIsOccas() {
        return isOccas;
    }

    public void setOccas(boolean occas) {
        isOccas = occas;
    }

    @Override
    public String toString() {
        return "Car : \n" +
                "mileage ='" + this.getMileage() + "'\n" +
                "brand ='" + this.getBrand() + "'\n" +
                "model ='" + this.getModel() + "'\n" +
                "engine ='" + this.getEngine() + "'\n" +
                "doors ='" + this.doors + "'\n" +
                "gears ='" + this.gears + "'\n" +
                "stock ='" + this.stock + "'\n" +
                "stock ='" + this.stock + "'\n" +
                "fuel ='" + this.getFuel() + "'\n" +
                "brakePower ='" + this.getBrakePower();
    }
}
