package com.Animals;

public class Cat extends Animal {
    private String name = "Chat";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void action() {
        System.out.println("Le chat miaule");
    }
}
