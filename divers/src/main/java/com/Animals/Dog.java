package com.Animals;

public class Dog extends Animal {
    private String name = "Chien";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void action() {
        System.out.println("Le chien aboie");
    }
}
