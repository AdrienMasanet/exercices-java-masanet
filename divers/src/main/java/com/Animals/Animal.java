package com.Animals;

public abstract class Animal {
    abstract public void action();
    abstract public String getName();
    abstract public void setName(String name);
}
