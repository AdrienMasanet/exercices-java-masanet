import javax.sound.midi.*;

public class Music {
    public static void main(String[] args) {
        Music inPlay = new Music();
        inPlay.play();
    }

    void play() {
        try {
            Sequencer sequencer = MidiSystem.getSequencer();
            sequencer.open();
            Sequence seq = new Sequence(Sequence.PPQ, 4);
            Track track = seq.createTrack();

            ShortMessage a = new ShortMessage();
            a.setMessage(144, 2, 38, 100);
            MidiEvent noteOn1 = new MidiEvent(a, 1);
            track.add(noteOn1);

            ShortMessage b = new ShortMessage();
            b.setMessage(128, 2, 38, 100);
            MidiEvent noteOff1 = new MidiEvent(b, 1);
            track.add(noteOff1);

            ShortMessage c = new ShortMessage();
            c.setMessage(144, 4, 50, 80);
            MidiEvent noteOn2 = new MidiEvent(c, 1);
            track.add(noteOn2);

            ShortMessage d = new ShortMessage();
            d.setMessage(128, 4, 50, 95);
            MidiEvent noteOff2 = new MidiEvent(d, 1);
            track.add(noteOff2);

            sequencer.setSequence(seq);
            sequencer.start();
            System.out.println("Lalalalaaa");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
