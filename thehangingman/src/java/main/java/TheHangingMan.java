package main.java;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class TheHangingMan {

    // Logic variables
    private final String wordsTextFilePath = Objects.requireNonNull(getClass().getResource("/thehangingman_words.txt")).getPath();
    private int lifePoints;
    private String wordToGuess;
    private String foundLettersUntilNow;
    private final List<Character> givenLetters = new ArrayList<>();

    // Images resources
    BufferedImage life8;
    BufferedImage life7;
    BufferedImage life6;
    BufferedImage life5;
    BufferedImage life4;
    BufferedImage life3;
    BufferedImage life2;
    BufferedImage life1;
    BufferedImage life0;

    // GUI elements
    JFrame mainFrame;
    JPanel mainPanel;
    JLabel drawLabel;
    JPanel drawPanel;
    JLabel foundLettersUntilNowLabel;
    JPanel foundLettersUntilNowPanel;
    JLabel infosLabel;
    JPanel infosPanel;
    JTextField letterTextField;
    JPanel letterTextFieldPanel;
    JButton tryLetterButton;
    JPanel tryLetterButtonPanel;

    public TheHangingMan() {
        try {
            life8 = ImageIO.read(new File(Objects.requireNonNull(getClass().getResource("/8.png")).getPath()));
            life7 = ImageIO.read(new File(Objects.requireNonNull(getClass().getResource("/7.png")).getPath()));
            life6 = ImageIO.read(new File(Objects.requireNonNull(getClass().getResource("/6.png")).getPath()));
            life5 = ImageIO.read(new File(Objects.requireNonNull(getClass().getResource("/5.png")).getPath()));
            life4 = ImageIO.read(new File(Objects.requireNonNull(getClass().getResource("/4.png")).getPath()));
            life3 = ImageIO.read(new File(Objects.requireNonNull(getClass().getResource("/3.png")).getPath()));
            life2 = ImageIO.read(new File(Objects.requireNonNull(getClass().getResource("/2.png")).getPath()));
            life1 = ImageIO.read(new File(Objects.requireNonNull(getClass().getResource("/1.png")).getPath()));
            life0 = ImageIO.read(new File(Objects.requireNonNull(getClass().getResource("/0.png")).getPath()));
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public static void main(String[] args) {
        TheHangingMan game = new TheHangingMan();
        game.initGui();
        game.setLifePoints(8);
        Scanner input = new Scanner(System.in);
        game.setWordToGuess(game.getRandomWordFromFile());
        game.syncFoundLettersUntilNow();
        game.refreshFoundLettersDisplay();

        System.out.println("Bienvenue dans The Hanging Man !\nLe mot à deviner est composé de " + game.getWordToGuess().length() + " lettres");
        /*
        do {
            System.out.println("========================================================================================");
            System.out.println("Vous disposez de " + game.getLifePoints() + " points de vie");
            game.syncFoundLettersUntilNow();
            game.printFoundLettersUntilNow();
            game.printGivenLetters();
            String response = input.nextLine();
            response = response.toLowerCase();

            if (response.length() > 1) {
                System.out.println("Vous devez rentrer une seule lettre !");
            } else if (response.length() == 0) {
                System.out.println("Vous devez saisir une lettre !");
            } else {

                // Rajoute la letre donnée à la liste de lettres déjà données si elle ne s'y trouve pas encore
                game.addLetterToGivenLetters(response);

                // Évalue si lettre présente dans le mot ou si perdre point de vie
                if (game.getLowerCaseWordToGuess().contains(response)) {
                    System.out.println("La lettre est bien dans le mot !");
                    game.syncFoundLettersUntilNow();
                } else {
                    game.decreaseLifePoints();
                    System.out.println("Ouch !.. La lettre n'est pas dans le mot..");
                    System.out.println("- 1 point de vie sur " + game.getLifePoints() + " restants !");
                }

                // Vérifie les points de vie et enclenche un game over si égal à 0
                if (game.getLifePoints() == 0) {
                    System.out.println("Vous n'avez plus de points de vie !");
                    System.out.println("Le mot était : " + game.getWordToGuess());
                    System.out.println("---------------------");
                    System.out.println("------- PERDU -------");
                    System.out.println("---------------------");
                    return;
                }

            }

        } while (!Objects.equals(game.getFoundLettersUntilNow(), game.getWordToGuess()));

        System.out.println("Bravo !!! Vous êtes parvenu à gagner et à échapper de peu au fait de mourir dans d'attroces souffrances");
        */
    }

    public void initGui() {
        // Main window
        mainFrame = new JFrame("The Hanging Man");
        mainFrame.setPreferredSize(new Dimension(640, 600));
        centerWindow(mainFrame);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.pack();
        mainFrame.setLocationRelativeTo(null);

        // Draw label
        drawLabel = new JLabel();
        drawLabel.setLayout(new BoxLayout(drawLabel, BoxLayout.X_AXIS));

        // Draw panel
        drawPanel = new JPanel();
        drawPanel.setLayout(new BoxLayout(drawPanel, BoxLayout.X_AXIS));
        drawPanel.setBorder(new EmptyBorder(20, 20, 20, 20));
        drawPanel.add(drawLabel);

        // Found letters until now label
        foundLettersUntilNowLabel = new JLabel();
        foundLettersUntilNowLabel.setFont(new Font("Sans-serif", Font.PLAIN, 26));
        foundLettersUntilNowLabel.setMaximumSize(foundLettersUntilNowLabel.getPreferredSize());

        // Found letters until now panel
        foundLettersUntilNowPanel = new JPanel();
        foundLettersUntilNowPanel.setLayout(new BoxLayout(foundLettersUntilNowPanel, BoxLayout.X_AXIS));
        foundLettersUntilNowPanel.setBorder(new EmptyBorder(5, 0, 5, 0));
        foundLettersUntilNowPanel.add(foundLettersUntilNowLabel);

        // Infos label
        infosLabel = new JLabel("Bienvenue dans The Hanging Man ! Rentrez une lettre pour commencer.");
        infosLabel.setMaximumSize(infosLabel.getPreferredSize());

        // Infos panel
        infosPanel = new JPanel();
        infosPanel.setLayout(new BoxLayout(infosPanel, BoxLayout.X_AXIS));
        infosPanel.setBorder(new EmptyBorder(5, 0, 5, 0));
        infosPanel.add(infosLabel);

        // Letter input field
        letterTextField = new JTextField();
        letterTextField.setColumns(1);
        letterTextField.setMaximumSize(letterTextField.getPreferredSize());

        // Letter input field panel
        letterTextFieldPanel = new JPanel();
        letterTextFieldPanel.setLayout(new BoxLayout(letterTextFieldPanel, BoxLayout.X_AXIS));
        letterTextFieldPanel.setBorder(new EmptyBorder(20, 0, 5, 0));
        letterTextFieldPanel.add(letterTextField);

        // Submit letter button
        tryLetterButton = new JButton("Tester la lettre");
        tryLetterButton.addActionListener(new submitLetter());

        // Submit letter button panel
        tryLetterButtonPanel = new JPanel();
        tryLetterButtonPanel.setLayout(new BoxLayout(tryLetterButtonPanel, BoxLayout.X_AXIS));
        tryLetterButtonPanel.setBorder(new EmptyBorder(5, 0, 20, 0));
        tryLetterButtonPanel.add(tryLetterButton);

        // Main window panel
        mainPanel = new JPanel();
        mainFrame.getContentPane().add(mainPanel);
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        mainPanel.add(drawPanel);
        mainPanel.add(foundLettersUntilNowPanel);
        mainPanel.add(infosPanel);
        mainPanel.add(letterTextFieldPanel);
        mainPanel.add(tryLetterButtonPanel);

        mainFrame.setVisible(true);
    }

    public static void centerWindow(Window frame) {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
        frame.setLocation(x, y);
    }

    private class submitLetter implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {

            String response = letterTextField.getText().toLowerCase();

            if (response.length() > 1) {
                setInfoText("Attention, vous ne devez rentrer qu'une seule lettre. Réessayez !");
            } else if (response.length() == 0) {
                setInfoText("Attention, n'avez pas rentré de lettre. Réessayez !");
            } else {
                addLetterToGivenLetters(response);

                if (getLowerCaseWordToGuess().contains(response)) {
                    if (!foundLettersUntilNow.contains(response)) {
                        setInfoText("La lettre \"" + response + "\" était bien dans le mot !");
                        syncFoundLettersUntilNow();
                        refreshFoundLettersDisplay();
                    } else {
                        setInfoText("Vous avez déjà trouvé la lettre \"" + response + "\" !");
                    }
                } else {
                    decreaseLifePoints();
                    setInfoText("Ouch !.. La lettre \"" + response + "\" n'est pas dans le mot.. " + getLifePoints() + " points de vie restants !");
                }

                // Vérifie les points de vie et enclenche un game over si égal à 0
                if (getLifePoints() == 0) {
                    setInfoText("Vous n'avez plus de points de vie ! Le mot était : " + getWordToGuess());
                }
            }

            letterTextField.setText("");
        }
    }

    public void setInfoText(String newText) {
        infosLabel.setText(newText);
        infosLabel.setHorizontalAlignment(SwingConstants.CENTER);
    }

    public void refreshFoundLettersDisplay() {
        foundLettersUntilNowLabel.setText(foundLettersUntilNow.replaceAll(".(?!$)", "$0 "));
        foundLettersUntilNowLabel.setMaximumSize(foundLettersUntilNowLabel.getPreferredSize());
        foundLettersUntilNowLabel.setHorizontalAlignment(SwingConstants.CENTER);
    }

    public void printGivenLetters() {
        if (givenLetters.size() > 0) {
            System.out.println("Lettres tentées : " + givenLetters);
        }
    }

    public void addLetterToGivenLetters(String response) {
        Character letter = response.charAt(0);
        givenLetters.add(letter);
    }

    public int getLifePoints() {
        return lifePoints;
    }

    public void setLifePoints(int lifePoints) {
        this.lifePoints = lifePoints;

        switch (this.lifePoints) {
            case 8 -> drawLabel.setIcon(new ImageIcon(life8));
            case 7 -> drawLabel.setIcon(new ImageIcon(life7));
            case 6 -> drawLabel.setIcon(new ImageIcon(life6));
            case 5 -> drawLabel.setIcon(new ImageIcon(life5));
            case 4 -> drawLabel.setIcon(new ImageIcon(life4));
            case 3 -> drawLabel.setIcon(new ImageIcon(life3));
            case 2 -> drawLabel.setIcon(new ImageIcon(life2));
            case 1 -> drawLabel.setIcon(new ImageIcon(life1));
            case 0 -> drawLabel.setIcon(new ImageIcon(life0));
        }
    }

    public void decreaseLifePoints() {
        setLifePoints(lifePoints - 1);
    }

    public String getWordToGuess() {
        return wordToGuess;
    }

    public String getLowerCaseWordToGuess() {
        return wordToGuess.toLowerCase();
    }

    public void setWordToGuess(String wordToGuess) {
        this.wordToGuess = wordToGuess;
    }

    public String getFoundLettersUntilNow() {
        return foundLettersUntilNow;
    }

    public void printFoundLettersUntilNow() {
        System.out.println(foundLettersUntilNow);
    }

    public void syncFoundLettersUntilNow() {
        StringBuilder newFoundLettersUntilNow = new StringBuilder();

        for (int i = 0; i < wordToGuess.length(); i++) {

            // Si la lettre du mot à deviner à l'itération "i" n'est pas un espace, alors on traite la lettre en question
            if (wordToGuess.charAt(i) != ' ') {
                // Si la lettre du mot à deviner à l'itération "i" fait partie des lettres données, recréer le mot trouvé jusqu'à maintenant
                if (givenLetters.contains(Character.toLowerCase(wordToGuess.charAt(i)))) {
                    newFoundLettersUntilNow.append(wordToGuess.charAt(i));
                } else {
                    newFoundLettersUntilNow.append("_");
                }
            } else {
                newFoundLettersUntilNow.append(" ");
            }

        }

        foundLettersUntilNow = newFoundLettersUntilNow.toString();
    }

    public String getRandomWordFromFile() {

        List<String> words = new ArrayList<>();

        try (FileInputStream inputStream = new FileInputStream(wordsTextFilePath)) {
            Scanner scanner = new Scanner(inputStream);

            while (scanner.hasNextLine()) {
                String word = scanner.nextLine();
                words.add(word);
            }

        } catch (Exception exception) {
            System.out.println("Erreur : " + exception.getMessage());
        }

        Random wordsRandomPicking = new Random();
        return words.get(wordsRandomPicking.nextInt(words.size()));

    }
}